package ubank.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import ubank.first_task.R;
import ubank.utils.RVItemDecorator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setupRV((RecyclerView)findViewById(android.R.id.list));
    }

    private void setupRV(RecyclerView rv) {
        rv.setAdapter(new RVAdapter());
        rv.setHasFixedSize(true);
        rv.addItemDecoration(new RVItemDecorator(getResources().getDimensionPixelSize(R.dimen.base_margin)));
    }
}
