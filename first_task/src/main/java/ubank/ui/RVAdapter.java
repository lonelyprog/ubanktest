package ubank.ui;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ubank.first_task.R;


class RVAdapter extends RecyclerView.Adapter<RVAdapter.VH> {
    private static final int N_ITEMS = 1000;
    private static final int MULTIPLIER = 3;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm dd.MM", Locale.getDefault());

    RVAdapter() {
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item, parent, false));
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        String lipsum = holder.itemView.getContext().getString(R.string.lorem_ipsum);
        String content = lipsum.substring(0, Math.min(lipsum.length(), (position + 1) * MULTIPLIER));
        holder.text.setText(content);
        holder.time.setText(DATE_FORMAT.format(new Date()));
    }

    @Override
    public int getItemCount() {
        return N_ITEMS;
    }

    static class VH extends RecyclerView.ViewHolder {
        TextView text;
        TextView time;

        VH(View parent) {
            super(parent);
            text = (TextView)parent.findViewById(R.id.text);
            time = (TextView)parent.findViewById(R.id.time);
        }
    }
}
