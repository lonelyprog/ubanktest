package ubank.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import ubank.mvp.view.MessageView;


@InjectViewState
public class MessagePresenter extends MvpPresenter<MessageView> {
    public MessagePresenter() {
    }
}
