package ubank.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ubank.mvp.view.MainView;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private static final int SECONDS_TO_WAIT = 5;

    public MainPresenter() {
    }

    public void onDismissMessage() {
        getViewState().hideMessage();
    }

    public void onButtonClick() {
        setupTimer(SECONDS_TO_WAIT);
    }

    private Disposable setupTimer(final int seconds) {
        return newTimer(seconds)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(next -> {},
                    Throwable::printStackTrace,
                    () -> getViewState().showMessage());
    }

    private Observable<Long> newTimer(int seconds) {
        return Observable.interval(1, TimeUnit.SECONDS).take(seconds);
    }
}