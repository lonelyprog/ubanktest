package ubank.mvp.view;

public interface MainView extends MessageView {
    void showMessage();
    void hideMessage();
}