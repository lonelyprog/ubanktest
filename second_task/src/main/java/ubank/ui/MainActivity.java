package ubank.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import ubank.mvp.presenter.MainPresenter;
import ubank.mvp.view.MainView;
import ubank.second_task.R;

public class MainActivity extends MvpAppCompatActivity implements MainView, MessageDialog.MessageCallbacks {
    private static final String DIALOG_TAG = "dialog";

    @InjectPresenter
    public MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        findViewById(R.id.btn_add_timer).setOnClickListener(v -> mainPresenter.onButtonClick());
    }

    @Override
    public void showMessage() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentByTag(DIALOG_TAG) == null) {
            fm.beginTransaction()
                    .add(MessageDialog.newInstance(getString(R.string.app_name),
                            getString(R.string.message)), DIALOG_TAG)
                    .commitNow();
        }
    }

    @Override
    public void hideMessage() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(DIALOG_TAG);
        if (fragment != null) {
            fm.beginTransaction()
                    .remove(fragment)
                    .commitNow();
        }
    }

    @Override
    public void onDismissMessage() {
        mainPresenter.onDismissMessage();
    }
}