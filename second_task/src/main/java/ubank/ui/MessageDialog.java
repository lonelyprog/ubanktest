package ubank.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import ubank.mvp.presenter.MessagePresenter;
import ubank.mvp.view.MessageView;


public class MessageDialog extends MvpAppCompatDialogFragment implements MessageView {
    interface MessageCallbacks {
        void onDismissMessage();
    }

    @InjectPresenter
    MessagePresenter presenter;

    private static final String ARG_TITLE = "title";
    private static final String ARG_MESSAGE = "message";

    private MessageCallbacks callbacks;

    public static MessageDialog newInstance(String title, String text) {
        MessageDialog dialog = new MessageDialog();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, text);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, getTheme());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callbacks = (MessageCallbacks)context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling activity must implement " +
                MessageCallbacks.class.getSimpleName() + " interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(getArguments().getString(ARG_TITLE))
                .setMessage(getArguments().getString(ARG_MESSAGE))
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (callbacks != null) {
            callbacks.onDismissMessage();
        }
    }
}
